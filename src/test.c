#define _DEFAULT_SOURCE

#include "test.h"

int free_heap(void* heap, size_t length) {
    return munmap(heap, size_from_capacity((block_capacity) { .bytes = length }).bytes);
}

void* _test_start(size_t test_number) {
    printf(">>> Test #%zu started...\n", test_number);
    void* heap = heap_init(capacity_from_size((block_size) { REGION_MIN_SIZE }).bytes);
    return heap;
}

void _test_end(size_t test_number, void* data_to_destroy, void* heap_to_destroy) {
    _free(data_to_destroy);
    free_heap(heap_to_destroy, REGION_MIN_SIZE);
    printf(">>> Test #%zu passed...\n", test_number);
}

void _test_failed_inform(size_t test_number) {
    fprintf(stderr, "!!! Test #%zu failed\n", test_number);
}

bool _test_simple_memory_allocation() {

    size_t test_number = 1;

    void* heap = _test_start(test_number);

    if (!heap) {
        _test_failed_inform(test_number);
        return false;
    }

    void* data = _malloc(1024);

    if (!data) {
        _test_failed_inform(test_number);
        return false;
    }

    _test_end(test_number, data, heap);
    return true;
}

bool _test_free_one_block_from_many_allocated() {
    size_t test_number = 2;

    void* heap = _test_start(test_number);

    if (!heap) {
        _test_failed_inform(test_number);
        return false;
    }

    void* data1 = _malloc(1024);
    void* data2 = _malloc(1024);

    _free(data1);


    if (!data1 || !data2) {
        _test_failed_inform(test_number);
        return false;
    }

    _test_end(test_number, data2, heap);
    return true;
}

bool _test_free_two_block_from_many_allocated() {
    size_t test_number = 3;
    void* heap = _test_start(test_number);

    if (!heap) {
        _test_failed_inform(test_number);
        return false;
    }

    void* data1 = _malloc(1024);
    void* data2 = _malloc(1024);
    void* data3 = _malloc(1024);

    if (!data1 || !data2 || !data3) {
        fprintf(stderr, "Test #3 failed!\n");
        return false;
    }

    _free(data2);
    _free(data1);

    if (!data3) {
        _test_failed_inform(test_number);
        return false;
    }

    _test_end(test_number, data3, heap);
    return true;
}

bool _test_grow_heap() {
    size_t test_number = 4;
    void* heap = _test_start(test_number);

    if (!heap) {
        _test_failed_inform(test_number);
        return false;
    }

    void* data = _malloc(REGION_MIN_SIZE * 2);

    struct block_header const* header = heap;
    if (data != HEAP_START + offsetof(struct block_header, contents)
        || header->capacity.bytes <= REGION_MIN_SIZE) {
        _test_failed_inform(test_number);
        return false;
    }

    _test_end(test_number, data, heap);

    return true;
}

bool _test_grow_heap_in_new_place() {
    size_t test_number = 5;
    void* heap = _test_start(test_number);

    (void)mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    void* data = _malloc(1024 * 10);

    if (!data) {
        _test_failed_inform(test_number);
        return false;
    }

    _test_end(test_number, data, heap);

    return true;
}