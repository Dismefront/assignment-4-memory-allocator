#include "test.h"

bool run_tests() {
    return _test_simple_memory_allocation()
        && _test_free_one_block_from_many_allocated()
        && _test_free_two_block_from_many_allocated()
        && _test_grow_heap()
        && _test_grow_heap_in_new_place();
}

int main() {
    printf(run_tests() ? "ALL TESTS PASSED\n" : "TESTS FAILED\n");
    return 0;
}