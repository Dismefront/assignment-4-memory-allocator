#ifndef TEST_H
#define TEST_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>

bool _test_simple_memory_allocation();

bool _test_free_one_block_from_many_allocated();

bool _test_free_two_block_from_many_allocated();

bool _test_grow_heap();

bool _test_grow_heap_in_new_place();

#endif